#!/usr/bin/env python3

import sys
import os
import csv
import math
import operator

def get_language (corpus):
    return (corpus.split("-")[0][3:])

def norm(c):
    # L2 norm
    return math.sqrt(sum(map(lambda x: x**2, c)))

def cosine(c1, c2):
    # 1st vector (without first 2 CSV columns)
    first = [int(x) for x in list(c1.values())[2:]]

    # 2nd vector (without first 2 CSV columns)
    second = [int(x) for x in list(c2.values())[2:]]

    # dotproduct / norm
    return sum(map(operator.mul, first, second)) / (norm(first) * norm(second))

def load (corpus_csv):
    with open(corpus_csv, newline='') as csvfile:
        reader = csv.DictReader(csvfile, dialect='excel-tab')
        # all lines
        corpus_list = list (reader)
        # filter above the 1K sentence threshold
        corpus_1K = [c for c in corpus_list if int (c["# sentences"]) >= 1000]

        # filter null vectors
        corpus_not_null = list(filter(lambda x: any(map(int, list(x.values())[2:])), corpus_1K))

    return corpus_not_null

def language_cosine(language_corpora):
    smallest = 1
    for c1 in language_corpora:
        for c2 in language_corpora:
            cos = cosine(c1,c2)
            # print ("(%s/%s) --> %f" % (c1["Corpus"], c2["Corpus"], cos))
            if cos < smallest:
                smallest = cos
    return smallest

if len (sys.argv) == 2:
    data = load (sys.argv[1])
    all_languages = list(dict.fromkeys([get_language (c["Corpus"]) for c in data]))
    output = []
    for language in all_languages:
        language_corpora = [c for c in data if get_language (c["Corpus"]) == language]
        if len(language_corpora) > 1:
            output += [(language, len(language_corpora), language_cosine(language_corpora))]
    sorted_output = sorted(output, key = lambda x: x[2])
    print("Language,CosineMin")
    for item in sorted_output:
        print ("%s(%d),%f" % (item[0], item[1], item[2]))

elif len (sys.argv) == 3:
    data = load (sys.argv[1])
    language_corpora = [c for c in data if get_language (c["Corpus"]) == sys.argv[2]]
    for c1 in language_corpora:
        for c2 in language_corpora:
            cos = cosine(c1,c2)
            print (f'{c1["Corpus"]},{c2["Corpus"]},{cos}')

else:
    print ("Error: one argument expected")



#for language in [
 #    'Hindi', 'Arabic', 'Romanian', 'Latin', 'German', 'French', 'Ancient_Greek', 'Czech',
  #   'Dutch', 'Slovenian', 'Spanish', 'Estonian', 'Faroese','Norwegian', 'Russian', 'Polish', 'Galician',
   #  'Turkish', 'Italian', 'Persian', 'Icelandic',
    # 'Finnish', 'Swedish', 'English', 'Portuguese', 'Korean', 'Chinese', 'Indonesian', 'Japanese', ]:
     #language_cosine ("./counts_with_dsubj.tsv", language)
