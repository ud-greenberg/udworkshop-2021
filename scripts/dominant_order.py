#!/usr/bin/env python3
import sys
import csv
import argparse

K_CORPUS = 'Corpus'
K_TOTAL = '# sentences'
K_NDO = 'ndo'

def sort_dict_by_key(data, key):
    # Sort a dict by the given key in the reverse order
    return sorted(data, key = lambda x: float(x[key]), reverse=True)


def build_ratio_dict(pattern_x, pattern_y):
    # Pattern names
    label_x  = pattern_x[0]
    label_y  = pattern_y[0]

    # Pattern occurences
    occurence_x = int(pattern_x[1])
    occurence_y = int(pattern_y[1])

    # Compute ratio between pattern x et pattern y
    ratio = occurence_x / occurence_y if occurence_y > 0 else 0
    ratio_dict = {'order': label_x, 'compared_to': label_y, 'ratio': ratio}

    return ratio_dict


def compute_ratios(corpus):
    # Remove first column (corpus name)
    name = corpus.pop(K_CORPUS)

    # Remove second column (Number of sentences, unused)
    _ = corpus.pop(K_TOTAL)

    # Keep only 2 most frequent patterns
    first_patterns = sort_dict_by_key(corpus.items(), 1)[0:2]

    # Build the dictionnary with ratio and patterns
    ratio_dict = build_ratio_dict(first_patterns[0], first_patterns[1])

    return ratio_dict


def print_order(corpus, ratios_dict):
    order = ratios_dict['order']
    compared_to = ratios_dict['compared_to']
    ratio = ratios_dict['ratio']

    # Dominant Order cases
    if ratio >= 2:
        print(f'{corpus}, {order}, {ratio}')
    elif ratio > 0 and ratio < 0.5:
        print(f'{corpus}, {compared_to}, {1/ratio}')
    elif ratio == 0: # only 1 pattern
        print(f'{corpus}, {order}, {ratio}')
    # No Dominant Order
    else:
        print(f'{corpus}, {K_NDO} ({order}/{compared_to}), {ratio}')



def run(corpus_csv):
    with open(corpus_csv, newline='') as csvfile:
        corpus_list = csv.DictReader(csvfile, dialect='excel-tab')

        for item in corpus_list:
            # Keep corpus name
            corpus_name = item['Corpus']

            # Build ratio object
            ratios = compute_ratios(item)

            # Print computed order
            print_order(corpus_name, ratios)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    _ = parser.add_argument('--input', nargs='+', help='input file')

    options = parser.parse_args()

    if (not options.input):
        print(f'No file in input: --input option is required')
        sys.exit(0)


    run(options.input[0])
