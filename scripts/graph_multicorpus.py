#!/usr/bin/env python3

import sys
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import argparse


def run(input, output):
    data = pd.read_csv(input, sep=',')
    df = data.sort_values(by=['CosineMin'])

    plt.rcdefaults()
    fig, ax = plt.subplots()

    # Example data
    language = df['Language']
    y_pos = np.arange(len(df['Language']))
    threshold = df['CosineMin']

    ax.barh(y_pos, threshold, align='center')
    ax.set_yticks(y_pos)
    ax.set_yticklabels(language)
    ax.invert_yaxis()  # labels read top-to-bottom
    ax.set_xlabel('Minimum cosine value')
    #ax.set_title('How fast do you want to go today?')

    for index, value in enumerate(threshold):
        plt.text(value, index, ' %.4f' %value, color='black', va='center_baseline')

    plt.xlim(0, 1.2)
    plt.tight_layout()
    plt.savefig(output)
    plt.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    _ = parser.add_argument('--input', nargs='+')
    _ = parser.add_argument('--output', default='output.pdf')

    options = parser.parse_args()

    if(not options.input):
        print(f'No file in input: --input option is required')
        sys.exit(0)

    run(options.input[0], options.output)
