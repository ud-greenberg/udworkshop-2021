import math
import matplotlib.pyplot as plt
import seaborn as sns
import sys
import itertools
import argparse

def grouper(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return itertools.zip_longest(*args, fillvalue=fillvalue)

def create_heatmap(fic_csv, output):
    with open(fic_csv) as f:
        lines = f.readlines()
        cosine_list = []
        corpus_list = []
        cosine_matrix = []

        # [data] is a list of lines, each line is a list of columns
        data = [l.rstrip().split(",") for l in lines]
        nb_corpus = int(math.sqrt(len(data)))

        for item in data:
            cosine_list.append(round(float(item[-1]), 4))
            corpus_list.append(item[1])

        matrix = grouper(cosine_list, nb_corpus)

        for item in matrix:
            cosine_matrix.append(list(item))

    plt.figure(figsize=(9,6))
    fig = sns.heatmap(cosine_matrix,
                      annot =True,
                      fmt = "g",
                      xticklabels =[c[3:] for c in corpus_list[:nb_corpus]],
                      yticklabels =[c[3:] for c in corpus_list[:nb_corpus]],
                      #vmin = 0.90,
                      vmax = 1
                      )
    plt.yticks(rotation=0)
    # plt.xticks(rotation=45)

    plt.tight_layout()
    plt.savefig("%s.pdf" %output)

    # return fig.show()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    _ = parser.add_argument('--input', nargs='+')
    _ = parser.add_argument('--output', nargs='+')

    options = parser.parse_args()

    if(not options.input):
        print(f'No file in input: --input option is required')
        sys.exit(0)

    if(not options.output):
        print(f'No file in output: --output option is required')
        sys.exit(0)

    create_heatmap(*options.input, *options.output)

#if __name__ == '__main__':
#    if len(sys.argv) < 3:
#        print(f'Usage: {__file__}  <le fichier d\'entrée> <le fichier de sortie>')
#        sys.exit(1)

#    create_heatmap(sys.argv[1], sys.argv[2])
