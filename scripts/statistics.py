#!/usr/bin/env python3

import pandas as pd
import argparse


def get_statistics(input, nb_patterns, output):
    df = pd.read_csv(input, index_col=False, sep = "\t")
    df['Total'] = df.iloc[:, -nb_patterns:].sum(axis=1)
    for i in df.columns[2:nb_patterns+2]:
        df['Prop_%s' %i] = df['%s' %i]/df['Total']*100
    df = df.fillna(0)

    df.to_csv(r'%s' %output)

    return df


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    _ = parser.add_argument('--input', nargs='+')
    _ = parser.add_argument('--nb_patterns', type=int)
    _ = parser.add_argument('--output', nargs='+')

    options = parser.parse_args()

    if(not options.input):
        print(f'No file in input: --input option is required')
        sys.exit(0)

    if(not options.output):
        print(f'No file in output: --output option is required')
        sys.exit(0)

    get_statistics(*options.input, options.nb_patterns, *options.output)
