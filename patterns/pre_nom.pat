pattern {
  P[upos=ADP];
  G[upos=NOUN|PRON|PROPN];
  G -[1=case]-> P;
  P << G; 
}
without {
	P-[1=fixed|flat]->X
}
