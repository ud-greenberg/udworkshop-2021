pattern {
  N[upos=NOUN];
  G[upos=NOUN];
  N -[1=nmod]-> G;
  G -[case]-> P;
  N << G;
}
