pattern {
  V [upos=VERB];
  V -[1=nsubj]-> S; S [upos=NOUN|PROPN|PRON];
  V -[1=obj]-> O; O [upos=NOUN|PROPN|PRON];
  V << O; O << S
}

without { P [lemma="?"|"!"]; 
V -[punct]-> P;}
