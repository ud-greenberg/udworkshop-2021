# Corpus-based Language Universals Analysis using Universal Dependencies

This folder contains all the code needed to reproduce the experiments presented in the article "Corpus-based Language Universals Analysis using Universal Dependencies" (https://syntaxfest.github.io/syntaxfest21/proceedings/Quasy-2021.pdf).

We used UD version 2.7 and GREW version 1.6 (https://grew.fr/).

## Compute basic statistics

### Count patterns occurrences

With GREW, given a set of patterns and a set of corpora described in a JSON file (see https://grew.fr/doc/corpora/), a TSV table is built with the number of occurrences for each pattern in each corpus.

`grew count -patterns "patterns/adj_noun.pat patterns/noun_adj.pat" -i ud2.7_1k.json > data/counts/counts_adj_noun.tsv`


| Corpus                 | # sentences | adj_noun | noun_adj |
|------------------------|-------------|----------|----------|
| UD_Afrikaans-AfriBooms | 1934        | 2555     | 457      |
| ...                    | ...         | ...      | ...      |
| UD_Wolof-WTF           | 2107        | 0        | 20       |


We provide all the TSV files obtained with GREW in the folder `counts` in order to run the python scripts without running the GREW part.

### Compute frequencies

`python3 scripts/statistics.py --input data/counts/counts_adj_noun.tsv --nb_patterns 2 --output data/stats/stats_adj_noun.csv`

## Get dominant order

Compute the ratio rank1 / rank2. If it is greater than 2 then rank1 is the dominant order, else there is no dominant order (ndo is printed with Rank1 and Rank2 values)

`python3 scripts/dominant_order.py --input data/counts/counts_adj_noun.tsv`

## Multicorpora Languages Analysis

### Compute cosine value

Minimal cosine value for multicorpora languages:

`python3 scripts/cosine.py data/counts/counts_adj_noun.tsv > data/minimum_cosine/minCosine_adj_noun.csv`

All cosine values for pairs of corpora for one given language:

`python3 scripts/cosine.py data/counts/counts_adj_noun.tsv French > data/cosine_by_language/cosine_french_adj_noun.csv`


### Visualization with figures

Histogram with minimal cosine value for multicorpora languages:

`python3 scripts/graph_multicorpus.py --input data/minimum_cosine/minCosine_adj_noun.csv --output figures/adj_noun_cosineMin.pdf`

Heatmap with cosine values for pairs of corpora:

`python3 scripts/cosine.py --input data/cosine_by_language/cosine_french_adj_noun.csv --output figures/heatmap_french_adj_noun.pdf`
